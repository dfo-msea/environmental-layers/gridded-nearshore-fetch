## Fetch Geometry Analysis
## Created: Oct 2014
## Description: Script performs a fetch analysis using Geometry objects wherever possible.
## Version 0.1: Oct 2, 2014. Can handle points on land, or touching land (some/all fetch lengths will be 0). Erasing is done using geometry operators only for maximum efficiency (fetch lines and land polygons are converted to geometry).
## Version 0.2: Oct 5, 2014. Fetch lines are written to an output feature class with the Point_ID. This is written to disk after each point is completed, in order to ensure robustness. The original point input file is joined with the fetch lengths csv file and output to a feature class at the completion of point processing. Processing time is displayed in lines/second for each point.
## Version 0.3: Oct 6, 2014. Major update: This version runs one point at a time, instead of one fetch line at a time as in previous versions. Fetch lines for each point are generated and stored as a multi-part line geometry. The geometry "difference" operation is used to erase an entire set of lines for each point at one time. The difference operation is very efficient with a large number of fetch lines. e.g. a multi-line geometry containing 100 fetch lines takes only about 3x longer to run the difference operator than a geometry containing only 1 fetch line. This is SIGNIFICANTLY faster and performance  per line actually *improves* with more fetch lines per point. After the difference operation, fetch line segments touching the input point are selected, and the original bearing is extracted using trig. Bearing and distance are recorded in a python dictionary, the values of which are then appended to the output csv file. Fetch lines are appended to an in-memory workspace, every 2000 lines they are saved to disk.
## Version 0.4: Oct 8, 2014. Changes: File GDB is used for output. Cleanup of temp "in_memory" feature classes at the end of each loop to free up memory. Placed the geometry difference operator inside a try block since it has thrown a couple of errors. Changed the logic of the csv file output: it still writes to the csv file, one line for each point, and closes the csv file after each point, but it immediately re-opens the file to establish a lock.
## Version 0.5: Oct 30, 2014. Changes: The FetchPoints output feature class contains a Point_ID attribute that matches the Point_ID in the FetchLines feature class. Fetch lines are saved to disk every 10 points. Displays the number of points in the input file during preparation stage. If there are no geometry errors, the GeometryErrors and InputPoints feature classes will be deleted upon successful completion of the script.
## Version 0.6: May 10, 2018. Changes: Modified so that only output is the CSV file with fetch information. This was done to speed up processing. Also added a condition where if the # of user defined lines are collected for a point, then the loop breaks rather than continues to sceep up processing.

#Import required modules
import os
import sys
import math
import time
import datetime

# Usage string
usage = "<input point file> <input coastline (land areas) file> <working folder> <fetch interval definition> <bearing interval (degrees)> <number of fetch lines> <fetch length (in projected map units)>"
usageExample = "python FetchGeom.py D:/Projects/inpoints.shp D:/Projects/land_area.shp D:/Projects/fetchWorkingDir \"Fetch Line Interval (degrees)\" 5 72 200000"

if len(sys.argv) < 7:
	raise Exception, "Invalid number of parameters provided. \n\n" + "Usage: " + usage + "\n\n Example: " + usageExample


startTime = datetime.datetime.now()
print startTime
#A numeric timestamp to append to file names
timeStamp = startTime.strftime("%m%d_%H%M")
print "Starting the geoprocessor object..."
#Import arcpy object
import arcpy

arcpy.AddMessage("Background prep starting")
#Get command line arguments.
#input point Feature Class
ptFC = arcpy.GetParameterAsText(0)
#Land polygons Feature Class.
#TO OPTIMIZE PERFORMANCE, THE LAND POLYGONS SHOULD HAVE BEEN DISSOLVED (i.e. only one large multi-part feature!). If not, the script will do this automatically, but it will take a little extra time at the start.
landFC = arcpy.GetParameterAsText(1)
#Working folder
workingDir = arcpy.GetParameterAsText(2)
#Definition of how to create fetch lines
fetchDefString = arcpy.GetParameterAsText(3)
#Bearing interval
bearingInterval = int(arcpy.GetParameter(4))
#Number of fetch lines
userDesiredFetchLines = int(arcpy.GetParameter(5))
#Fetch distance
fetchDistance = int(arcpy.GetParameter(6))

if fetchDefString=="Fetch Line Interval (degrees)":
	arcpy.AddMessage("Creating " + str(int(360/bearingInterval)) + " fetch lines per point.")
elif fetchDefString=="Number of Fetch Lines per Point":
	bearingInterval = int(round(360/userDesiredFetchLines))
	arcpy.AddMessage("Number of fetch lines must be evenly divisible by 360. ")
	arcpy.AddMessage("Creating " + str(int(360/bearingInterval)) + " fetch lines per point.")
else:
	arcpy.AddError("Fourth parameter must be either \"Fetch Line Interval (degrees)\" or \"Number of Fetch Lines per Point\", try again.")
	sys.exit()

geoDBName = "Fetch.gdb"
geoDBLocation = os.path.join(workingDir,geoDBName)

#Set the working geodatabase
if not os.path.isdir(geoDBLocation):
    #A geodatabase hasn't been created yet
    arcpy.AddMessage("Creating output GDB in " + workingDir)
    arcpy.CreateFileGDB_management (workingDir,geoDBName)

arcpy.AddMessage("Working GDB: " + geoDBLocation)


#Set the working folder
arcpy.env.workspace = geoDBLocation
arcpy.env.overwriteOutput = True
arcpy.env.qualifiedFieldNames = False
arcpy.env.XYResolution = "0.00000001 Meters"

#Define the start and end bearings
startBearing = 0
endBearing = 359.999
### Create a list that will hold each of the Polyline objects (fetch lines) created around each point
##features = []
# Create an empty Geometry object. This is used to convert features into Geometry.
g = arcpy.Geometry()

#Get the projection of the land feature class, and use it as the base coordinate system.
arcpy.AddMessage( "Reading coordinate system of land feature class...")
coord_sys = arcpy.Describe(landFC).spatialReference
arcpy.env.outputCoordinateSystem = coord_sys

#Check the coordinate system of the point feature class. If it is not the same as the land features (used to set the environment), discontinue and exit.
arcpy.AddMessage( "Reading point file...")
ptCoordSys = arcpy.Describe(ptFC).spatialReference
if not (ptCoordSys.Name)==(coord_sys.Name):
	arcpy.AddError( "Point and land feature classes must have the same projection!")
	sys.exit()

#Create a File GDB copy of the input points, to assign extra precision and make sure OID starts at 1 instead of 0.
pointFile = arcpy.FeatureClassToFeatureClass_conversion(ptFC,geoDBLocation,"InputPoints_" + timeStamp)
ptFC = pointFile
countInputPoints = arcpy.GetCount_management(pointFile)
arcpy.AddMessage( str(countInputPoints) + " points read.")

#Prepare the land polygons for processing.
arcpy.AddMessage( "Preparing land polygons...")
landGeomList = arcpy.CopyFeatures_management(landFC, g)
#If the land polygons have been correctly dissolved to one single feature (as required for this script), there will only be one multipart geometry.
#print len(landGeomList)
if (len(landGeomList) > 1):
	#A temp name for the dissolved land polygons
	landDissolve = "land_dissolved" + timeStamp
	arcpy.AddWarning( "Land polygon must have only one multi-part feature for the entire land area." )
	arcpy.AddMessage( "Dissolving land polygons...")
	arcpy.Dissolve_management(landFC,landDissolve,"","","MULTI_PART")
	landGeomList = arcpy.CopyFeatures_management(landDissolve, g)
	arcpy.Delete_management(landDissolve)
	arcpy.AddMessage( "Dissolve complete.")

#This is the land geometry object that will be used for all "difference" operations to erase fetch line geometry.
landGeom = landGeomList[0]
arcpy.AddMessage( "There are " + str(landGeom.partCount) + " land polygons in " + str(len(landGeomList)) + " multi-part feature.")


#Create a temp feature class for the line features (in memory objects are used to reduce processing time). This will be used to assign the attributes for each fetch line, before copying the fetch lines for each point to the output feature class.
##linesTemp = "temp_lines"
##outFC = "FetchLines_" + timeStamp
geomErrName = "GeometryErrors_" + timeStamp
geomErrInMemoryName = "in_memory/GeomErrors_" + timeStamp
##linesTemp = arcpy.CreateFeatureclass_management("in_memory", "linesTemp" + timeStamp, "POLYLINE", "", "", "", coord_sys)
##arcpy.CreateFeatureclass_management(geoDBLocation, outFC, "POLYLINE", "", "", "", coord_sys)
geomErrors = arcpy.CreateFeatureclass_management(geoDBLocation, geomErrName, "POINT", "", "", "", coord_sys)
##arcpy.AddField_management(linesTemp,"Point_ID","LONG")
##arcpy.AddField_management(outFC,"Point_ID","LONG")



#Empty arcpy Point object
point = arcpy.Point()
#Empty arcpy array objects (to hold coordinate pairs for the line ends and multi-part lines)
array = arcpy.Array()
MultiPart = arcpy.Array()

#Prepare the output csv file header for calculated fetch lengths.
arcpy.AddMessage( "Preparing output files...")
outFileName = os.path.join(workingDir, "Fetch_" + timeStamp + ".csv")
outFile = open(outFileName, 'a+')
outData = "Point_ID,X,Y"
bearing = startBearing
while bearing <= endBearing:
	outData += ",bearing" + str(int(bearing))
	bearing = bearing + bearingInterval
outData += "\n"
#print outData
outFile.write(outData)
outFile.close()
outData = ""
#Open the output file and keep it open for the duration.
outFile = open(outFileName, 'a+')

#Initialize the dictionary to store the fetch distances, and the bearing list to store the list of bearings. Initialize the fetch distances to a default value of zero. (For edge conditions, where the point is exactly on the coastline, some fetch bearings may have a valid calculated distance of zero. Also for points that are inside the coastline polygons--i.e. on land--the fetch distances for all bearings will be zero.)
d = {}
bearingList = []
countFetchLines = 0

bearing = startBearing
while bearing <= endBearing:
	bearingList.append(bearing)
	d[bearing] = 0
	bearing = bearing + bearingInterval

#Calculate time required for preparation.
endTime = datetime.datetime.now()
timeDelta = (endTime - startTime).total_seconds()
arcpy.AddMessage("Finished preparation. " + str(timeDelta) + " seconds")

#Start processing each point, one at a time.
startTime = datetime.datetime.now()
lineTimer = datetime.datetime.now()
arcpy.AddMessage("\n*****************************************")
arcpy.AddMessage("Starting to process input points")
processedPoints = 0

for row in arcpy.da.SearchCursor(ptFC, ["OID@", "SHAPE@XY"]):

    #Get the x,y coordinates for the current point.
    oid = row[0]
    #print str(oid)
    x, y = row[1]
    #Create a point geometry object.
    ptGeom = arcpy.PointGeometry(arcpy.Point(x,y))

    #Write the OID, x and y coordinates to the output string
    outData = ""
    outData += str(oid) + "," + str(x) + "," + str(y)

    #Initialize the bearing
    bearing = startBearing
    #Iterate through the bearings and create a fetch line for each bearing. Store the fetch lines as parts of a multi-part line geometry.
    while bearing <= endBearing:

    	#Calculate the end points of the current fetch line, based on the bearing and source x,y coordinates.
    	a = math.radians(bearing)
    	deltaY = fetchDistance*(math.cos(a))
    	deltaX = fetchDistance*(math.sin(a))
    	x2 = x + deltaX
    	y2 = y + deltaY

    	#Get the START point of the bearing line
    	array.add(arcpy.Point(x,y))

    	#Get the END point of the bearing line
    	point.X = x2
    	point.Y = y2
    	array.add(point)

    	#Add a line segment to the multi-part array. This will eventually contain one line part for each bearing.
    	MultiPart.add(array)

    	array.removeAll()
    	#Increment the bearing, move on to the next fetch line.
    	bearing = bearing + bearingInterval
    	countFetchLines = countFetchLines + 1

    #Create a polyline geometry using the array of calculated line endpoints. Assign a projection, "coord_sys" which has already been defined.
    currentLine = arcpy.Polyline(MultiPart,coord_sys)
    #reset the multi-part line array
    MultiPart.removeAll()

	#Erase the current set of fetch lines using the difference operator on the land Geometry.
    t = datetime.datetime.now()
    try:
    	lineErased = currentLine.difference(landGeom)
    except:
    	arcpy.AddMessage("\nGeometry difference error on point " + str(oid))
    	rejectedPoint = arcpy.PointGeometry(arcpy.Point(x, y))
    	geomErrorsInMemory = arcpy.CopyFeatures_management(rejectedPoint, geomErrInMemoryName)
    	#Save the rejected point to a file GDB feature class
    	arcpy.Append_management(geomErrorsInMemory, geomErrors)
    	raw_input("Press Enter to continue...")
    	continue

    el = ( datetime.datetime.now() - t).total_seconds()
    print "\n" + str(el) + " sec geometry difference. " + str(lineErased.partCount) + " parts."

    # counter for number of line segments touching point
    num_lines = 0

	#Now the erase operation for all fetch lines around one point is complete, in one step.
	#Assume that there are multiple parts to this erased line geometry. Iterate through all parts and find the ones that touch the original point geometry. Often, the highest part number contains the line segment touching the original point, so we'll start iterating from there and go backwards.

    t = datetime.datetime.now()
    for i in reversed(xrange(lineErased.partCount)):

		#get the current part of the erased line, convert it to a Polyline geometry
        currentLineSegment = arcpy.Polyline(lineErased.getPart(i))
        currentLineArray = lineErased.getPart(i)

        if currentLineSegment.touches(ptGeom):
##			#We found a fetch line segment touching the input point!
##			#Append this line segment to the list of features that will be written to the output feature class (fetch lines around each point).
##            features.append(currentLineSegment)
            # increment counter
            num_lines += 1

			#Disassemble the part, calculate its bearing.
            deltaX = currentLineArray[1].X - currentLineArray[0].X
            deltaY = currentLineArray[1].Y - currentLineArray[0].Y
            a = math.atan2(deltaX, deltaY)

			#Round off the bearing to the nearest integer so it matches the exiting list of bearings
            bearing = round(math.degrees(a))
            if (bearing < 0):
				#correct negative angles for quadrants where deltaX < 0
				bearing = 360 + bearing

			#Record the fetch distance for the current bearing in the fetch distance dictionary
            d[int(bearing)] = str(int(round(currentLineSegment.length)))

        # if we hit number of desired fetch line segments that touch point, break loop
        if num_lines == userDesiredFetchLines:
            print("{} lines touching source point found...".format(userDesiredFetchLines))
            break

	el = ( datetime.datetime.now() - t).total_seconds()
    print str(el) + " sec select touching fetch segments, calculate bearing/fetch dist."

	#We are now done with fetch distance calulcations for this point!
	#Write the fetch distances for this point to the output csv string. Reset the fetch distance dictionary at the same time.
    for b in bearingList:
    	#get the fetch distance stored in the dictionary under bearing b, convert to string, and append to the output csv string.
    	outData += "," + str(d[b])
    	#reset the dictionary
    	d[b] = 0

    #Add an EOL line break - goes to next row in csv file
    outData += "\n"

    #Write fetch distances for this point to disk (append to csv file).
    #outFile = open(outFileName, 'a+')
    outFile.write(outData)
    outFile.close()
    outData = ""
    #immediately re-open the file again, to establish a lock.
    outFile = open(outFileName, 'a+')

##	#If there are fetch lines in the features buffer to be written to a feature class, then add the attribute Point_ID to each one and append to the in-memory workspace (in case of a point that is entirely on land, the features buffer will be empty, and there will be no fetch lines)
##	if (len(features) > 0):
##		t = datetime.datetime.now()
##		#Copy the features[] polyline array to a new feature class. This step is necessary because you cannot append an array of line features using Append_management. It has to go through a copy first.
##		fetchTmp = arcpy.CopyFeatures_management(features, "in_memory/fetch" + timeStamp)
##		arcpy.AddField_management(fetchTmp,"Point_ID","LONG")
##		#Update the attributes for Point_ID
##		with arcpy.da.UpdateCursor(fetchTmp,["Point_ID"]) as cursor:
##			for row in cursor:
##				row[0] = int(oid)
##				cursor.updateRow(row)
##
##		el = ( datetime.datetime.now() - t).total_seconds()
##		print str(el) + " sec updated " + str(len(features)) + " fetch lines with Point_ID"
##
##		t = datetime.datetime.now()
##		arcpy.Append_management(fetchTmp, linesTemp)
##
##		#Cleanup
##		features = []
##		arcpy.Delete_management(fetchTmp)
##		el = ( datetime.datetime.now() - t).total_seconds()
##		print str(el) + " sec append fetch lines to in-memory workspace"

    #Finished a point. Calculate the timing.
    pointTime = (datetime.datetime.now() - lineTimer).total_seconds()
    linesPerSec = (360 / bearingInterval)/pointTime
    #Reset the timer.
    lineTimer = datetime.datetime.now()
    arcpy.AddMessage("Point " + str(oid) + " completed at " + str("%.2f" % linesPerSec) + " lines/sec.")

    #Increment the number of processed points
    processedPoints = processedPoints + 1

##	#If there are 10 or more points completed, then dump the existing fetch lines to disk.
##	if (processedPoints > 9):
##		arcpy.AddMessage("\nBatch of 10 points completed. Saving " + str(countFetchLines) + " fetch lines to disk")
##		#Save the temp fetch lines from in-memory to disk
##		arcpy.Append_management(linesTemp, outFC, "NO_TEST")
##		#Delete all features from the temp feature class
##		arcpy.DeleteFeatures_management(linesTemp)
##		#Reset counters
##		countFetchLines = 0
##		processedPoints = 0


#Finished processing points.
endTime = datetime.datetime.now()
timeDelta = (endTime - startTime).total_seconds()
arcpy.AddMessage("\nFinished processing points. " + str(timeDelta) + " seconds")
#Close the output file, finally.
outFile.close()

###Save any remaining temp fetch lines from in-memory to disk
##countFetchLines = arcpy.GetCount_management(linesTemp)
##if (countFetchLines > 0):
##	arcpy.AddMessage("Saving remaining fetch lines")
##	arcpy.Append_management(linesTemp, outFC, "NO_TEST")

t = datetime.datetime.now()
###Join the output csv file with the input points, then export to a final shapefile.
##arcpy.AddMessage("Joining fetch table to final output...")
##featureLayerName =  "ptsToJoin" + timeStamp
##arcpy.MakeFeatureLayer_management(ptFC, featureLayerName)
##oidField = arcpy.Describe(ptFC).OIDFieldName
##arcpy.AddJoin_management(featureLayerName, oidField, outFileName, "Point_ID")
##arcpy.CopyFeatures_management(featureLayerName, os.path.join(geoDBLocation, "FetchPoints_" + timeStamp))
el = ( datetime.datetime.now() - t).total_seconds()
print str(el) + " sec final join"

#If there are no Geometry Errors, delete the error file.
countGeomErrors = str(arcpy.GetCount_management(geomErrors))
if countGeomErrors == "0":
    #If there were no geometry errors, delete the GeometryErrors and InputPoints feature classes. Otherwise, keep them both to help track down the source of error.
    arcpy.AddMessage("No geometry errors found.")
    arcpy.Delete_management(geomErrors)
    arcpy.Delete_management(ptFC)
else:
	arcpy.AddMessage("There were " + str(countGeomErrors) + " points with geometry errors.")

#Cleanup stray data
##arcpy.Delete_management(linesTemp)
finish = datetime.datetime.now()
print finish
