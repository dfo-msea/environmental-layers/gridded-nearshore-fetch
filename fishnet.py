#########################################################################################################################################################
#-------------------------------------------------------------------------------------------------------------------------------------------------------#
'''
 NAME:          fishnet.py
 PURPOSE:       Makes fishnet grid (polygon) and clips grid to the feature that was used to create the extent for the fishnet grid.

 MODULES:       arcpy

 DATE:          March 28, 2018

 AUTHOR:        Cole Fields
                Spatial Analyst
                Marine Spatial Ecosystem Analysis (MSEA)
                Fisheries and Oceans Canada
'''
#-------------------------------------------------------------------------------------------------------------------------------------------------------#

## IMPORT MODULES
import arcpy
from arcpy import env

## DISSOLVED BOTTOM PATCHES
bottom_patches = r"D:\Projects\Fetch\Data\HG_Bottom_Patches_5kmBufferClip.shp"
extent = arcpy.Describe(bottom_patches).extent
spatial_ref = arcpy.Describe(bottom_patches).spatialReference

## SET ENVIRONMENT
env.workspace = r"D:\Projects\Fetch\Data\HG_Bottom_Patches.gdb"
arcpy.env.extent = arcpy.Extent(extent.XMin, extent.YMin, extent.XMax, extent.YMax)
arcpy.env.outputCoordinateSystem = spatial_ref

## FISHNET VARIABLES
# output feature class
outFeatureClass = "hg_bottom_patches_grid_50m"

# set origin of fishnet
originCoordinate = '{} {}'.format(extent.XMin, extent.YMin)

# set orientation (same X as origin, Y is max rather than min)
yAxisCoordinate = '{} {}'.format(extent.XMin, extent.YMax)

# set cell size
cellSizeWidth = 50
cellSizeHeight = 50

# set number rows and columns - NOT REQUIRED AS CELL SIZE IS SUPPLIED
numRows =  ""
numColumns = ""

# set opposite corner - NOT REQUIRED
oppositeCoorner = ""

# create point feature class with labels at center of each cell - NOT REQUIRED AS POINTS ARE GENERATED AFTER CLIP OPERATION
labels = "NO_LABELS"

# Extent is set by origin and opposite corner - no need to use a template fc
templateExtent = '#'

# set geom type
geometryType = "POLYGON"

## CREATE FISHNET
print("Creating {} m fishnet grid with extent from {}...".format(cellSizeHeight, bottom_patches))
fishnet = arcpy.CreateFishnet_management(outFeatureClass, originCoordinate, yAxisCoordinate, cellSizeWidth, cellSizeHeight, numRows, numColumns, oppositeCoorner, labels, templateExtent, geometryType)

## CLIP FISHNET TO DISSOLVED BOTTOM PATCHES
print("Clipping {} to {}...".format(str(fishnet), bottom_patches))
clipped_fishnet = arcpy.Clip_analysis(fishnet, bottom_patches, outFeatureClass + "_clip")

## GET GRID CELLS THAT INTERSECT BOTTOM PATCHES, BUT DO NOT CLIP THEM - EXPORT AS SEPARATE FEATURE CLASS - THESE CELLS WILL GET THE FETCH VALUES ATTACHED (ONCE PROCESSED) AND CONVERTED TO RASTER
print("Making feature layer for selection analysis...")
arcpy.MakeFeatureLayer_management(fishnet, "fishnet_lyr")

print("Selecting grid cells that overlap {}...".format(bottom_patches))
selection = arcpy.SelectLayerByLocation_management("fishnet_lyr", "INTERSECT", bottom_patches)

print("Exporting grid selection as feature class...")
no_clip = arcpy.CopyFeatures_management(selection, outFeatureClass + "_selection_noClip")

## CREATE CENTROID POINTS FROM CLIPPED GRID - USE "INSIDE" FOR point_location PARAMETER
print("Creating centroid points from grid cells...")
fishnet_points = arcpy.FeatureToPoint_management(clipped_fishnet, outFeatureClass + "_points", "INSIDE")
