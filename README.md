# gridded-nearshore-fetch

__Main author:__  Michael Peterman    
__Affiliation:__  Fisheries and Oceans Canada (DFO)   
__Group:__        Marine Spatial Ecology and Analysis   
__Location:__     Institute of Ocean Sciences   
__Contact:__      e-mail: Cole.Fields@dfo-mpo.gc.ca | tel: 250-363-8060


- [Objective](#objective)
- [Summary](#summary)
- [Status](#status)
- [Contents](#contents)
- [Methods](#methods)
- [Requirements](#requirements)
  + [Modules](#modules)
  + [Parameters](#parameters)
- [Caveats](#caveats)
- [Uncertainty](#uncertainty)
- [Acknowledgements](#acknowledgements)
- [References](#references)


## Objective
The purpose of these scripts is to generate gridded points, output csv files 
with fetch estimates for point locations, and calculate the sum and minimum 
for each record. The final csv with coordinate and fetch estimates can be
made into a spatial dataset (shapefile/feature class) and then converted into
a raster surface.


## Summary
This script calculates fetch values for input points. For each point, lines are
generated radiating out. The user decides on the length of the lines and the
number of fetch lines. For example, the user may generate lines with a 200,000
metre length every 5 degress (72 lines per point). The lines are clipped to the
input land barrier file. The length of lines are in the projected map units.
Sum fetch is the total length of all the lines. Minimum fetch would be the 
distance to shore (along any one of the fetch lines). The output is a csv file
where each record corresponds to one input point and has all the fetch values
for each bearing.


## Status
Completed


## Contents
 Script | Step | Description 
 --- | --- | --- 
fishnet.py | 1: Create gridded points | Makes fishnet grid (polygon) and clips grid to the feature that was used to create the extent for the fishnet grid. Creates gridded points covering the fishnet grid.
FetchGeom_CSV_only.py | 2: Calculate fetch estimates for points | Creates estimates of fetch for each input point and outputs a csv file with records for each point.
fetchSumMin.R | 3: Calculate min & sum fetch values | Takes directory where csv(s) from FetchGeom_CSV_only.pt and calculates minimum value and sum value for calculated fetch values for all bearing fields. R works fast with tables, so this was written in R to merge multiple csv files and run calculations. 


## Methods
1. Generate input points that fetch values will be calculated for. These can 
either be created only along the shoreline at a certain interval or covering
the extent of a polygon. fishnet.py will generate gridded points covering an 
input polygon. The points are positioned at the centroid of the grid cell, 
except along the edges of the polygon. Here, points are snapped to the edges.
If using a polygon that is meant to have an edge along the coastline, ensure
that the input barrier file is aligned with this boundary. Otherwise, there will
be discrepencies (i.e. points on land).

2. Create/prep land barrier file. The more complex the geometry, the longer the
processing will take when running the fetch geometry tool. To reduce computing 
time, the land barrier file can be clipped (ensure that the clip allows enough 
land to account for the length of lines created around the points). The input 
coastline may be simplified as well. However, if it is simplified and points 
were generated across the extent of polygons, that edge of that boundary needs 
to override the simplified coast edge.

3. Run FetchGeom_CSV_only.py to create csv files with coordinates and calculated fetch
values for all bearings. 

4. If sum of fetch and minimum fetch are desired and the csv files are very 
large, fetchSumMin.R can be run in R to quickly calculate sum and min fetch 
values.

5. If gridded points were created covering a polygon, the points can be converted
to raster with desired value (sum and min) in ArcGIS using feature to raster. 
It is suggested to smooth the output raster using a focal mean with a 
neighbourhood of cells.


## Requirements
### Modules
`ArcPy` python

`sp` R

`dplyr` R


### Parameters

Script | Parameters | Notes
--- | --- | ---
fishnet.py | 1. Input vector shape that will be used to create a clipped grid and points created inside the grid. <br> 2. Workspace file geodatabase for output spatial data <br>3. Output feature class name | Hardcoded into script
FetchGeom_CSV_only.py | 1. input point file (path to input points) <br> 2. input coastline (land areas) file (path to input land barriers file) <br> 3. working folder (path for working directory) <br> 4. fetch interval definition (Fetch Line Interval (degrees)) <br> 5. bearing interval (degrees) (5)  <br> 6. number of fetch lines (72)  <br> 7. fetch length (in projected map units) (200000) | Run from command line
fetchSumMin.R | 1. Set working directory where csv files are <br>2. Set output file name (i.e. fetch.csv) | Hardcoded into script

## Caveats
Anything other users should be aware of including gaps in the input or output data and warnings about appropriate use.


## Uncertainty
Fetch values are estimates only, and are useful as a relative index of exposure. 


## Acknowledgements
Cole Fields, Jessica Nephin, Edward Gregr (SciTech Consulting)


## References
https://www.gis-hub.ca/dataset/fetch-resources
